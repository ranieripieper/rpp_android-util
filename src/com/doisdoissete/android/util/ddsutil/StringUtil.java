package com.doisdoissete.android.util.ddsutil;

import java.text.Normalizer;

public class StringUtil {

	public static String getNormalizeStr(String state) {
		if (state != null) {
			state = Normalizer.normalize(state, Normalizer.Form.NFD);
			state = state.replaceAll("[^\\p{ASCII}]", "");
			state = state.toLowerCase().replaceAll(" ", "_");
		}
		
		return state;
	}
}
