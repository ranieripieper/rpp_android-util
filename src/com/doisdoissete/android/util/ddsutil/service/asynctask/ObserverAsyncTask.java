package com.doisdoissete.android.util.ddsutil.service.asynctask;

public abstract class ObserverAsyncTask<T> {

	public abstract void onPreExecute();
	public abstract void onPostExecute(T result);
	public abstract void onCancelled();
	public abstract  void onError(Exception e);
}
