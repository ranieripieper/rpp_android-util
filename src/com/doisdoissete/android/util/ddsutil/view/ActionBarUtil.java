package com.doisdoissete.android.util.ddsutil.view;

import android.content.res.Resources;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.R;

public class ActionBarUtil {

	public static void hideHomeAsUp(Window window) {
		
		View v = window.getDecorView().getRootView();
		int upId = Resources.getSystem().getIdentifier("up", "id", "android");
		if (upId > 0) {
		    final ImageView up = (ImageView) v.findViewById(upId);
		    //up.setImageDrawable(new ColorDrawable(window.getContext().getResources().getColor(android.R.color.transparent)));
		    if (up != null) {
		    	up.setImageResource(R.drawable.ic_blank);
		    }
		    
		}
	}
}
