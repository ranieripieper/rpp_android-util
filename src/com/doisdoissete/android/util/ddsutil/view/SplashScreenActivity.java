package com.doisdoissete.android.util.ddsutil.view;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

public abstract class SplashScreenActivity extends Activity {

	private static int SPLASH_TIME_OUT = 3000;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        setContentView(getContentView());

        new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				startMainActivity();
				 finish();
			}
		}, getSplashScreenTimeOut());
        
    }
    
    protected int getSplashScreenTimeOut() {
    	return SPLASH_TIME_OUT;
    }
    
    protected abstract int getContentView();
	protected abstract void startMainActivity();
}
