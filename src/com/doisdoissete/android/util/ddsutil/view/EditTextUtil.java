package com.doisdoissete.android.util.ddsutil.view;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class EditTextUtil {

	public static final void hiddenKeyboard(Context mContext, EditText edt) {
		if (edt != null) {
			InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
		}
	}
}
