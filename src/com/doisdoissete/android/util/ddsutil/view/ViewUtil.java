package com.doisdoissete.android.util.ddsutil.view;

import android.view.View;
import android.view.ViewGroup;

public class ViewUtil {
	
	public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if ( view instanceof ViewGroup ) {
            ViewGroup group = (ViewGroup)view;

            for ( int idx = 0 ; idx < group.getChildCount() ; idx++ ) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }
	
}
