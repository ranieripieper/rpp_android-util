package com.doisdoissete.android.util.ddsutil.exception;

public class ParseException extends Exception {

	private static final long serialVersionUID = 1L;
	private String msgErro;
	
	public ParseException() {
		
	}
	
	public ParseException(String m) {
		super();
		this.msgErro = m;
	}
	
	public ParseException(Exception e) {
		super(e);
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	
}