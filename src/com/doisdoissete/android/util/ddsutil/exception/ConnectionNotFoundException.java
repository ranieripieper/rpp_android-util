package com.doisdoissete.android.util.ddsutil.exception;

public class ConnectionNotFoundException extends ConnectionException {

	private static final long serialVersionUID = 1L;
	private String msgErro;
	
	public ConnectionNotFoundException() {
		
	}
	
	public ConnectionNotFoundException(String m) {
		super();
		this.msgErro = m;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	
}