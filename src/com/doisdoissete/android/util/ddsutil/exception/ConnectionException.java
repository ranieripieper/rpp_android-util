package com.doisdoissete.android.util.ddsutil.exception;

import org.springframework.http.HttpStatus;

public class ConnectionException extends Exception {

	private static final long serialVersionUID = 1L;
	private String msgErro;
	private HttpStatus httpStatus;
	
	public ConnectionException() {
		
	}
	
	public ConnectionException(String m) {
		super();
		this.msgErro = m;
	}
	
	public ConnectionException(Exception e) {
		super(e);
	}
	
	public ConnectionException(Exception e, String msg, HttpStatus httpStatus) {
		super(e);
		this.msgErro = msg;
		this.httpStatus = httpStatus;
	}
	
	public ConnectionException(String msg, HttpStatus httpStatus) {
		this.msgErro = msg;
		this.httpStatus = httpStatus;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	
}