package com.doisdoissete.android.util.ddsutil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static final ThreadLocal<DateFormat> dfDiaMesAno = new ThreadLocal<DateFormat>() {
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat("dd/MM/yyyy");
		}
	};
	
	public static final ThreadLocal<DateFormat> dfDiaMesAnoHoraMinuto = new ThreadLocal<DateFormat>() {
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat("dd/MM/yyyy '-' HH'h'mm");
		}
	};
	
	public static final ThreadLocal<DateFormat> dfAnoMesDiaHifen = new ThreadLocal<DateFormat>() {
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd");
		}
	};
	
	public static final int getDifDias(Date dtInit, Date dtFim) {
		return (int)((dtFim.getTime() - dtInit.getTime())/ (24*60*60*1000));
	}
	
	public static final int getDifHoras(Date dtInit, Date dtFim) {
		return (int)((dtFim.getTime() - dtInit.getTime())/ (60*60*1000));
	}
	
	public static final int getDifMinutes(Date dtInit, Date dtFim) {
		return (int)((dtFim.getTime() - dtInit.getTime())/ (60*1000));
	}
	
	public static final int getDifSeconds(Date dtInit, Date dtFim) {
		return (int)((dtFim.getTime() - dtInit.getTime())/ (1000));
	}
}
